# Pipenv Guide

앞서 [Pipenv](https://sdldocs.gitlab.io/pipenv/)를 작성하였지만 보다 읽기 편한 문서 [Pipenv: A Guide to the New Python Packaging Tool](https://realpython.com/pipenv-guide/#whats-next)를 찾아 이를 편역하벼 보기로 하였다. 이를 먼저 읽고 난 다음, 보다 더 자세한 설명을 위하여 앞의 [문서](https://sdldocs.gitlab.io/pipenv/)를 참고하기 바란다.

이는 [Pipenv: A Guide to the New Python Packaging Tool](https://realpython.com/pipenv-guide/#whats-next)를 슈어데이터랩 환경에 적용할 수 있도록 편역한 것이다.

