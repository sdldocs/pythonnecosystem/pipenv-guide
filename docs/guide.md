# Pipenv Guide

## Pipenv가 해결하려는 문제 <a id="problems"></a>

Pipenv의 이점을 이해하려면 Python의 packaging 및 종속성 (또는 의존성) 관리(dependency management) 방법을 살펴보는 것이 중요하다.

먼저 서드파티 패키지를 취급하는 일반적인 상황부터 살펴보자. 다음 완전한 Python 어플리케이션을 배포하기 위한 방법을 구축한다.

### `requirements.txt`를 이용한 종속성 관리 <a id="dependency-management-with-requirementstxt"></a>
`flask`와 같은 서드파티 패키지를 사용하는 Python 프로젝트를 진행하고 있다고 가정 하자. 다른 개발자가 자동화된 시스템에서 어플리케이션을 실행할 수 있도록 해당 요건을 지정해야 한다.

따라서 `flask` 종속성을 `requirements.txt` 파일에 포함하기로 하였다.
```
flask
```

로컬에서는 모든 것이 잘 작동하며, 어플리케이션을 더 개발한 다음 실제 운영하기로 하였다. 여기서 오류가 있을 것같은 느낌이 온다.

위  `requirements.txt` 파일에 사용하는 `flask` 버전이 지정되지 않았다. 이 경우 `pip install -r requirements.txt`는 기본적으로 최신 버전을 설치한다. 최신 버전에서 응용 프로그램을 손상시키는 인터페이스 또는 동작 변경이 없는 한 문제가 발생하지 않는다.

예를 들어, 개발 시 사용한 버전과는 하위 호환성이 없는 새로운 버전의 `flask`가 출시된 상황이 발생하였다고 하자.

이제 어플리케이션을 실제 운영 환경에 배포하고 `pip install -r requirements.txt`을 실행하면 [`pip`](https://realpython.com/what-is-pip/)은 하위 호환성이 없는 최신 버전의 `flask`를 사용한다. 그러면 어플리케이션은 실제 운영시 작동되지 않는다.

*"하지만 내 컴퓨터에선 작동했어!"* 나도 실행해 보았지만 뭔가 석연치 않았다.

현시점에서는, 개발시에 사용한 `flask` 버전이 정상적으로 동작하고 있는 것을 알 수 있다. 문제를 해결하려면 `requirements.txt`을 좀 더 구체적으로 작성해야 한다. 이를 위하여 `flask` 종속성에 `버전 지정자(version specifier)`를 추가한다. 이를 종속성 *고정(pinning)*이라고 한다.
```python
flask==0.12.1
```

`flask` 종속성을 특정 버전으로 고정하면 `pip install -r requirements.txt`의 요구 사항이 보장된다. 개발 중에 사용한 `flask`의 정확한 버전을 설정한다. 하지만 정말 그럴까?

`flask` 자체에도 종속성이 있다는 점을 간과하지 말아야 한다 (`pip`이 `flask`가 사용하는 패키지를 자동으로 설치한다). 왜냐하면 `flask` 자체는 그 종속성에 대한 정확한 버전을 명시하지 않는다. 예를 들어, `Werkzeug>=0.14`인 임의의 버전을 사용할 수 있다.

이 예를 위해 새로운 버전의 `Werkzeug`가 출시되어 어플리케이션에서 [show-stopper 버그](https://www.techopedia.com/definition/22054/showstopper-bug)가 발생했다고 가정해 보면, `pip install -r requirements.txt`를 실행할 때 이번 운영 환경에서는 고정된 요구 사항은 `flask==0.12.1`이다. 하지만 안타깝게도 최신 버전의 `Werkzeug`를 사용할 수 밖에 없다. 다시, 운영 과정에서 중단될 수 있다.

여기서 진짜 문제는 빌드가 유동적이라는 것이다. 즉, 같은 입력(`requirements.txt`)이 주어졌지만, `pip`이 항상 같은 환경을 생성하지 않는다. 현 프로덕션시점에서 개발 머신과 동일한 환경을 간단하게 복제할 수 없다.

이 문제의 일반적인 해결책은 `pip freeze`를 사용하는 것이다. 이 명령을 사용하면 `pip`으로 설치된 하위 종속성 포함하여 자동으로 현재 설치된 모든 서드파티 라이브러리의 정확한 버전을 가져올 수 있다. 따라서 개발 중인 환경을 동결하여 동일한 환경을 프로덕션 환경으로 유지할 수 있다.

`pip freeze`를 실행하면 그 결과 `requirements.txt`에 고정된 종속성을 추가한다.
```
click==6.7
Flask==0.12.1
itsdangerous==0.24
Jinja2==2.10
MarkupSafe==1.0
Werkzeug==0.14.1
```

이러한 고정된 종속성을 통해 프로덕션 환경에 설치된 패키지가 개발 환경의 패키지와 정확히 일치하는 것을 보장할 수 있으므로 제품이 예기치 않게 중단되지 않도록 할 수 있다.  그러나 불행히도 이 "해결책"은 완전히 새로운 문제를 야기한다.

이제 각 서드파티 패키지의 정확한 버전을 지정했으므로 `flask`의 하위 종속성이 있더라도 이러한 버전을 최신 상태로 유지해야 한다. `Werkzeug==0.14.1`에서 보안 취약점이 발견되어 패키지 유지 관리자들이 `Werkzeug==0.14.2`으로 즉시 패치를 적용하면 어떻게 될까? 패치되지 않은 이전 버전의 `Werkzeug`에서 발생하는 보안 문제를 방지하려면 `Werkzeug==0.14.2`로 업데이트해야만 한다.

먼저 현재 버전에 문제가 있음을 인지해야 할 것이다. 그런 다음 다른 사용자가 보안 취약점을 악용하기 전에 프로덕션 환경에서 새 버전을 사용하여야 한다. 그래서`requirements.txt`에 새 버전 `Werkzeug==0.14.2`를 지정하도록 수동으로 변경해야 한다. 이 상황에서 알 수 있듯이 개발자는 필요한 업데이트를 최신 상태로 유지해야 하는 책임을 갖게 된다.

그러나 어떤 버전의 `Werkzeug`가 설치되더라도 운영상에 중단이 없는 한 아무도 신경 쓰지 않는다는 것이다. 실제로 버그 수정, 보안 패치, 새로운 기능, 더 많은 최적화 등을 위하여 최신 버전이 필요할 수 있다.

결국 질문은 다음과 같습니다. "**어떻게 하면 하위 종속성 버전을 업데이트할 책임을 개발자가 지지 않고 Python 프로젝트를 위한 정적인 빌드를 만들 수 있을까?**"

> 스포일러 경고: `Pipenv` 사용이 간단한 해결책이다.

### 서로 다른 종속성을 갖는 프로젝트 개발 환경 <a id="development-of-projects-with-different-dependencies"></a>
이제 여러 프로젝트를 동시에 수행할 때 발생하는 또 다른 일반적인 문제에 대해 살펴보겠다. 프로젝트 A에는 `django==1.9`가 필요하지만 프로젝트 B에는 `django==1.10`이 필요하다고 상상해 보자.

기본적으로 Python은 모든 서드파티 패키지를 시스템 전체에 저장하려고 한다. 즉, `Project A`와 `Project B`간 전환이 일어날 때마다 적절한 버전의 `django`가 설치되어 있는지 확인해야 한다. 따라서 각 프로젝트의 요구 사항을 충족하기 위해 패키지를 제거하고 다시 설치해야 하므로 프로젝트 간 전환이 불편하다.

표준 솔루션은 자체 Python 실행 파일 및 서드파티 패키지 스토리지가 갖는 [가상환경(virtual environment)](https://sdldocs.gitlab.io/python-virtual-environments/)을 사용하는 것이다. 이렇게 하면 `Project A`와 `Project B`를 적절히 격리할 수 있다. 이제 프로젝트 간에 동일한 패키지 저장 위치를 공유하지 않기 때문에 쉽게 전환할 수 있다. `Project A`는 자체 환경에서 필요한 어떤 버전의 `django`도 가질 수 있으며 `Project B` 또한 완전히 다른 버전을 가질 수 있다. 이를 위한 일반적인 도구로 `virtualenv`(Python 3에서는 `venv`)가 있다.

가상 환경 관리 기능이 내장되어 있어 패키지 관리를 위한 `Pipenv`를 단일 도구로 사용할 수 있다.

### 종속성 해결 <a id="dependency-resolution"></a>
종속관계 해결이란 무엇일까? 다음과 같은 'requirements.txt` 파일이 있다고 가정하자.
```
package_a
package_b
```

`package_a`는 `package_c`에 종속적이고 (즉, `package_a`에서 `package_c`를 호출) `package_a`는 `package_c`의 특정 버전 `package_c>=1.0`가 필요하다고 가정한다. 또한 `package_b`는 동일한 하위 종속성을 가지지만 `package_c <=2.0`이 필요하다.

`package_a` 및 `package_b`를 설치하려고 하면 설치 도구는 `package_c`(>=1.0 및 <=2.0인)의 요건을 확인하고 이러한 요건을 충족하는 버전을 선택한다. 툴이 의존관계를 해결하여 최종적으로 프로그램이 동작하도록 하는 것이 바람직할 것이다. 이것이 '종속성 해결'이 의미하는 것이다.

아쉽게도 현재 `pip` 자체로 이같은 실질적인 종속성 해결 방법을 지원하지 않고 있지만 이를 지원하고자 [공개 이슈](https://github.com/pypa/pip/issues/988)로 남겨 놓고 있다.

`pip`은 위의 시나리오를 다음과 같은 방법으로 처리한다.

1. `package_a`를 설치하고 최초의 요건(`package_c>=1.0`)을 만족하는 `package_c` 의 버전을 검색한다.
2. 그런 다음 해당 요건을 충족하기 위해 `package_c`의 최신 버전을 설치한다. 이때 `package_c`의 최신 버전이 3.1이라고 하자.

여기서 (잠재적으로) 문제가 시작된다.

`pip`에 의해 선택된 `package_c` 버전이 향후 요건에 맞지 않으면 (예를 들어 `package_b`의 요건은 `package_c <=2.0`임 등) 설치가 실패된다.

이 문제의 '해결책'은 `requirements.txt`에 필요한 서브종속성(`package_c`) 범위를 지정하는 것이다. 이렇게 하면 `pip`은 이 상충을 해결할 수 있는 요건을 충족하는 패키지를 설치할 수 있다.
```
package_c>=1.0,<=2.0
package_a
package_b
```

이전과 마찬가지로 하위 종속성(`package_c`)에 대해 개발자가 직접 개입하고 있다. 문제는 `package_a`가 사용자 모르게 요건을 변경했을 경우 지정한 요건(`package_c>=1.0,<=2.0`)이 받아들여지지 않아 설치가 다시 실패할 수 있다는 것이다. 실질적인 문제는 하위 종속성에 대한 요구 사항을 최신 상태로 유지해야 한다는 것이다.

이상적으로는 서브 종속성 버전을 명시적으로 지정하지 않아도 모든 요건을 충족하는 패키지를 설치할 수 있도록 하는 것이다.

## Pipenv 소개 <a id="pipenv-introduction"></a>
문제를 살펴보았으니 `Pipenv`가 어떻게 문제를 해결하는지 살펴보겠다.

먼저, 다음과 같이 설치한다.
```shell
$ pip install pipenv
```

`Pipenv`는 기본적으로 대체 기능을 하기 때문에 이 작업을 완료하면 `pip`에 대해 효과적으로 잊을 수 있다. 또한 두 개의 새로운 파일인 [`Pipfile`](https://github.com/pypa/pipfile)(`requirements.txt`의 대체 파일)과 `Pipfile.lock`(배포 빌드를 지원)를 소개한다.

`Pipenv`는 내부적으로 `pip`과 `virtualenv`를 사용하지만 하나의 명령으로 그 사용을 단순하였다.

### 사용 예 <a id="example-usgae"></a>
멋진 Python 어플리케이션을 만드는 것부터 시작하자. 먼저 가상 환경에서 쉘을 생성하여 이 어플리케이션의 개발을 분리한다.
```shell
$ pipenv shell
```

가상 환경이 아직 존재하지 않는 경우 이를 통해 가상 환경을 생성한다. `Pipenv`는 모든 가상 환경을 기본 장소(현 디렉토리)에 생성한다. `Pipenv`의 기본 동작을 변경할 수 있는 [환경변수](https://docs.pipenv.org/advanced/#configuration-with-environment-variables)가 있다.

`--two`와 `--three` 인수를 각각 사용하여 Python 2 또는 3 환경을 강제로 생성할 수 있다. 그렇지 않으면 `Pipenv`는 기본 `virtualenv`를 사용한다.

> **Note**: 더 구체적인 버전의 Python이 필요한 경우 필요한 버전을 `--python` 인수로 지정할 수 있다. 예: `--python 3.6`

이제 필요한 서드파티 패키지 `flask`를 설치할 수 있다. 하지만 최신 버전이 아닌 버전 0.12.1이 필요하다는 것을 알고 있으므로 명확히 다음과 같이 기술한다.
```shell
$ pipenv install flask==0.12.1
```

단말기에 다음과 같이 출력된다.
```
Installing flask==0.12.1...
Adding flask to Pipfile's [packages]...
✔ Installation Succeeded
Pipfile.lock not found, creating...
Locking [dev-packages] dependencies...
Locking [packages] dependencies...
Building requirements...
Resolving dependencies...
✔ Success!
Updated Pipfile.lock (197c02)!
Installing dependencies from Pipfile.lock (197c02)...
  🐍   ▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉ 0/0 — 00:00:00
```

`Pipfile`과 `Pipfile.lock` 파일이 생성된다. 잠시 후 이를 자세히 살펴보도록 한다. 수치 계산를 위해 다른 서드파티 패키지 `numpy`를 설치한다. 특정 버전이 필요하지 않으므로 지정하지 않는다.

```shell
$ pipenv install numpy
```

버전 관리 시스템(VCS)으로 부터 직접 설치하고 싶은 경우 아래와 같이 할 수 있다. `pip`과 마찬가지로 저장소(repository)를 지정한다. 예를 들어 버전 관리 시스템이 요청한 라이브러리(request library)를 설치하려면 다음을 수행한다.
```shell
$ pipenv install -e git+https://github.com/requests/requests.git#egg=requests
```

설치를 변경하려면 위의 -e 인수를 수정한다. 현재, 이는 `Pipenv`가 서브종속성 해결을 위해서 [필요하다](https://pipenv.readthedocs.io/en/latest/basics/#a-note-about-vcs-dependencies).

이 어플리케이션을 위한 단위 테스트(unit test)도 포함하고 있고, [`pytest`](https://realpython.com/pytest-python-testing/)를 사용한다고 하자. 운영 환경에서 `pytest`는 필요하지 않으므로 `--dev` 인수를 사용하여 이 종속성이 개발에만 적용되도록 지정할 수 있다.
```shell
$ pipenv install pytest --dev
```

`--dev` 인수를 지정하면 `Pipfile`의 특별한 `[dev-packages]`에 종속 관계를 기록한다. 이러한 개발 패키지는 `pipenv install`시 `--dev` 인수를 지정한 경우에만 설치된다.

각 섹션은 개발에만 필요한 종속성과 실제 코드가 작동하기 위해 필요한 종속성을 구분한다. 일반적으로 이것은 `dev-requirements.txt` 또는 `test-requirements.txt`과 같은 추가 요건 파일을 사용하여 이루어진다. 이제 모든 것이 단일 `Pipfile`의 섹션들로 통합된다.

이제 개발자 자신의 개발 환경에서 모든 것을 사용할 수 있으며, 이를 운영 환경에 적용할 준비가 되었다고 가정해 보겠다. 그러기 위해서는 환경을 고정하여 개발 환경과 동일한 환경을 운영 환경에 구축해야 한다.

```shell
$ pipenv lock
```

이렇게 하여 `Pipfile.lock`이 작성되고 업데이트된다. 수동으로 편집할 필요가 없다 (또, 수동으로 편집할 의미도 없다). 항상 생성된 파일을 사용해야 한다.

이제 코드와 `Pipfile.lock` 파일을 프로덕션 환경으로 이동시켜 정상적으로 기록된 배포 환경을 구성해야 한다.
```shell
$ pipenv install --ignore-pipfile
```

그러면 `Pipenv`는 설치를 위해 `Pipfile`을 무시하고 `Pipfile.lock`에 있는 파일을 사용한다. 주어진 `Pipfile.lock`으로 `Pipenv`는 `pipenv lock`을 수행했을 때처럼, 서브종속성과 모두를 지닌 정확히 동일한 환경을 생성한다.

잠금 파일은 환경 내 모든 패키지 버전의 스냅샷을 생성하여 결정 빌드를 가능하게 한다 (`pip freeze`의 결과와 유사).

이제 다른 개발자가 코드를 추가하려고 하면, 이 경우 `Pipfile`을 포함한 코드를 취득하고 다음 명령을 사용합니다.

```shell
$ pipenv install --dev
```

위의 명령으로 개발에 필요한 모든 종속성이 설치된다. 그 결과 일반 종속성을 설치할 때 `--dev` 인수로 지정된 종속성을 모두 포함한다.

> `Pipfile`에 정확한 버전이 지정되어 있지 않은 경우 `install` 명령에 따라 종속성(및 하위 종속성) 버전을 업데이트할 수 있다.

앞서 논의한 문제들을 해결할 수 있기 때문에 위의 내용은 중요하다. 예를 들어, 종속 관계의 새로운 버전을 사용할 수 있게 되었다고 하자. 이 종속성의 특정 버전이 필요하지 않으므로 `Pipfile` 파일에 정확한 버전을 지정하지 않았다. `pipenv install`을 실행하면 종속관계의 새로운 버전을 개발 환경에 설치한다.

이제 코드를 변경하고 몇 가지 테스트를 수행하여 모든 것이 예상대로 작동하는지 확인한다 (단위 테스트도 포함). 이전과 마찬가지로 `pipenv lock`을 사용하여 환경을 고정하면 새로운 버전의 종속성을 사용하여 업데이트된 `Pipfile.lock`을 생성한다. 이전과 마찬가지로 잠금(lock) 파일을 사용하여 프로덕션에 새 환경을 복제할 수 있다.

이 시나리오에서 알 수 있듯이, 개발 환경과 실제 프로덕션 환경이 동일한지 확인하기 위해 실제로 필요하지 않은 버전을 더 이상 유지할 필요가 없다. 또한 "관련없는" 하위 종속성 업데이트에 대한 최신 정보를 확인할 필요도 없다. `Pipenv`를 이용한 워크플로우와 정밀한 테스트를 조합하면 모든 종속관계 관리를 수동으로 수행하여야 했던 문제가 해결된다.

### Pipenv의 종속성 해결 방식 <a id="pipenv-resolution-approach"></a>
`Pipenv`는 핵심 종속성의 모든 요건을 충족하는 하위 종속성의 설치를 시도한다. 단, 종속관계가 상충되는 경우(`package_a`는 `package_c>=1.0`, `package_b`는 `package_c<1.0`가 필요), `Pipenv`는 잠금파일을 작성할 수 없으며 다음과 같은 오류를 출력한다.
```
Warning: Your dependencies could not be resolved. You likely have a mismatch in your sub-dependencies.
  You can use $ pipenv install --skip-lock to bypass this mechanism, then run $ pipenv graph to inspect the situation.
Could not find a version that matches package_c>=1.0,package_c<1.0
```

경고에 나타나 있듯이 종속성 그래프를 만들어 최상위 종속성과 하위 종속성을 살펴볼 수도 있다.
```shell
$ pipenv graph
```

이 명령어는 다음 예와 같이 종속관계를 보이는 트리 같은 구조를 출력한다. 
```
Flask==0.12.1
  - click [required: >=2.0, installed: 6.7]
  - itsdangerous [required: >=0.21, installed: 0.24]
  - Jinja2 [required: >=2.4, installed: 2.10]
    - MarkupSafe [required: >=0.23, installed: 1.0]
  - Werkzeug [required: >=0.7, installed: 0.14.1]
numpy==1.14.1
pytest==3.4.1
  - attrs [required: >=17.2.0, installed: 17.4.0]
  - funcsigs [required: Any, installed: 1.0.2]
  - pluggy [required: <0.7,>=0.5, installed: 0.6.0]
  - py [required: >=1.5.0, installed: 1.5.2]
  - setuptools [required: Any, installed: 38.5.1]
  - six [required: >=1.10.0, installed: 1.11.0]
requests==2.18.4
  - certifi [required: >=2017.4.17, installed: 2018.1.18]
  - chardet [required: >=3.0.2,<3.1.0, installed: 3.0.4]
  - idna [required: >=2.5,<2.7, installed: 2.6]
  - urllib3 [required: <1.23,>=1.21.1, installed: 1.22]
```

`pipenv` 그래프 출력에서 이전에 설치한 최상위 종속성(`Flask`, `numpy`, `pytest` 및 `requests`)을 확인할 수 있으며, 그 아래에는 종속적 관계가 있는 패키지를 표시한다.

또한 트리를 뒤집어 필요한 부모가 필요한 하위 종속성을 표시할 수도 있다.
```shell
$ pipenv graph --reverse
```

이 역(reversed) 트리는 상충하는 하위 종속성을 파악하려 할 때 더 유용할 수 있다.

### Pipfile <a id="pipfile"></a>
[`Pipfile`](https://github.com/pypa/pipfile)은 `requirements.txt` 파일을 대체하는 것이다. 현재 Pipenv는 `Pipfile`을 사용하기 위한 참조 구현이다. [이 파일들을 `pip` 자체에서 처리](https://github.com/pypa/pipfile#pip-integration-eventual)할 수 있을 것 같다. 또한 [Pipenv는 Python 자체에서 추천하는 공식 패키지 관리 도구](https://packaging.python.org/tutorials/managing-dependencies/#managing-dependencies)이기도 하다.

`Pipfile`은 [TOML](https://github.com/toml-lang/toml) 구문으로 작성하며, 파일은 여러 섹션으로 나뉜다. `[dev-packages]`는 개발 전용 패키지의 경우, `[packages]`는 최소 필수 패키지의 경우, 특정 버전의 Python과 같은 기타 요구 사항의 경우 `[requires]`를 사용한다. 샘플 파일은 다음과 같다.
```toml
[[source]]
url = "https://pypi.python.org/simple"
verify_ssl = true
name = "pypi"

[dev-packages]
pytest = "*"

[packages]
flask = "==0.12.1"
numpy = "*"
requests = {git = "https://github.com/requests/requests.git", editable = true}

[requires]
python_version = "3.6"
```

이상적으로는 `Pipfile`에 하위 종속성이 없어야 한다. 즉, 실제로 개발된 코드에 `import`하여 사용하는 패키지만 포함하면 된다. `requests`의 하위 종속성이라는 이유만으로 `Pipfile`에 `chardet`을 유지할 필요가 없다 (Pipenv는 이들을 자동으로 설치한다). `Pipfile`은 패키지에 필요한 최상위 종속성을 전달해야 한다.

### Pipfile.lock <a id="pipfile-lock"></a>
이 파일은 환경을 재현하기 위한 정확한 요건을 지정하여 최종 배포 빌드를 만들 수 있도록 한다. 패키지 및 해시의 정확한 버전을 포함시켜 보다 안전한 검증을 지원한다. 이러한 검증은 이제 [`pip` 자체에서도 지원한다](https://pip.pypa.io/en/stable/reference/pip_install/#hash-checking-mode). 예제 파일은 다음과 같다. 이 파일의 구문은 JSON이며 파일의 일부를 생략하였다.
```json
{
    "_meta": {
        ...
    },
    "default": {
        "flask": {
            "hashes": [
                "sha256:6c3130c8927109a08225993e4e503de4ac4f2678678ae211b33b519c622a7242",
                "sha256:9dce4b6bfbb5b062181d3f7da8f727ff70c1156cbb4024351eafd426deb5fb88"
            ],
            "version": "==0.12.1"
        },
        "requests": {
            "editable": true,
            "git": "https://github.com/requests/requests.git",
            "ref": "4ea09e49f7d518d365e7c6f7ff6ed9ca70d6ec2e"
        },
        "werkzeug": {
            "hashes": [
                "sha256:d5da73735293558eb1651ee2fddc4d0dedcfa06538b8813a2e20011583c9e49b",
                "sha256:c3fd7a7d41976d9f44db327260e263132466836cef6f91512889ed60ad26557c"
            ],
            "version": "==0.14.1"
        }
        ...
    },
    "develop": {
        "pytest": {
            "hashes": [
                "sha256:8970e25181e15ab14ae895599a0a0e0ade7d1f1c4c8ca1072ce16f25526a184d",
                "sha256:9ddcb879c8cc859d2540204b5399011f842e5e8823674bf429f70ada281b3cc6"
            ],
            "version": "==3.4.1"
        },
        ...
    }
}
```

모든 종속성에 대해 정확히 지정된 버전을 기록해 두어야 한다. 이 `Pipfile.lock`에는 `Pipfile`에 없는 `werkzeug`와 같은 하위 종속성을 표시한다. 해시는 개발할 때 사용한 것과 동일한 패키지를 확인하는 데 사용된다.

이 파일을 수동으로 변경하지 말아야 하며, `pipenv lock` 명령을 사용하여 생성하여야 한다.

### Pipenv의 추가 기능 <a id="pipenv-extra-featues"></a>
다음 명령을 사용하여 기본 편집기에서 서드파티 패키지를 연다.
```shell
$ pipenv open flask
```
**Not working! why? But working with `pipenv open click`**
```
$ pipenv open Flask
Module not found!
```

```shell
$ pipenv open click
Opening '/home/yoonjoon.lee/.local/share/virtualenvs/Test_Pipenv-tbiG5VPn/lib/python3.8/site-packages/click' in your EDITOR.
```

기본 에디터에서 `flask` 패키지를 열수 있다. 이때 `EDITOR` 환경변수로 에티터를 지정할 수 있다. 예를 들어 [Sublime Text](https://www.sublimetext.com/)를 사용하려고 하면 `EDITOR=subl`을 설정하면 된다. 이것에 의해, 사용하고 있는 패키지의 내부를 매우 간단하게 살펴볼 수 있다.

쉘을 시작하지 않고 가상 환경에서 다음 명령을 실행할 수 있다.
```shell
$ pipenv run <insert command here>
```

사용자 환경의 보안 취약성(및 [PEP 508](https://www.python.org/dev/peps/pep-0508/) 요건)을 확인한다.
```shell
$ pipenv check
Checking PEP 508 requirements...
Passed!
Checking installed package safety...
44715: numpy <1.22.2 resolved (1.22.1 installed)!
Numpy 1.22.2  includes a fix for CVE-2021-41495: Null Pointer Dereference vulnerability exists in numpy.sort in NumPy in the PyArray_DescrNew function due to missing return-value validation, which allows attackers to conduct DoS attacks by repetitively creating sort arrays.
NOTE: While correct that validation is missing, an error can only occur due to an exhaustion of memory. If the user can exhaust memory, they are already privileged. Further, it should be practically impossible to construct an attack which can target the memory exhaustion to occur at exactly this place.
https://github.com/numpy/numpy/issues/19038
```

이제 더 이상 패키지가 필요하지 않다면 uninstall할 수 있다.
```shell
$ pipenv uninstall numpy
Uninstalling numpy...
No package numpy to remove from Pipfile.
Locking [dev-packages] dependencies...
Locking [packages] dependencies...
Building requirements...
Resolving dependencies...
✔ Success!
Updated Pipfile.lock (197c02)!
```

게다가 가상 환경에 설치한 모든 패키지를 모두 uninstall할 수 있다.
```shell
$ pipenv uninstall --all
```

--all을 --all-dev로 변경하면 개발 패키지만 uninstall할 수도 있다.

`.env` 파일이 최상위 디렉토리에 있는 경우 `Pipenv`는 환경변수을 자동으로 로드할 수 있다. 따라서 가상 환경을 열기 위해 `pipenv` 쉘을 사용하면 환경 변수가 파일에서 로드된다. `.env` 파일에는 키와 값 쌍만 기록하고 있다.
```
SOME_ENV_CONFIG=some_value
SOME_OTHER_ENV_CONFIG=some_other_value
```

마지막으로, 가상환경이 어디에 저장되어 있는지 알아보기 위한 몇 가지 명령어가 있다. 가상 환경의 경로를 다음과 같이 확인할 수 있다.
```shell
$ pipenv --venv
/home/yoonjoon.lee/.local/share/virtualenvs/Test_Pipenv-tbiG5VPn
```

프로젝트 홈의 경로를 찾는 방법은 다음과 같다.
```shell
$ pipenv --where
/home/yoonjoon.lee/Drills/Python/Test_Pipenv
```

## Package 배포 <a id="package-distribution"></a>
이 섹션에서는 개발된 코드를 패키지로 배포하는 방법에 대하여 알아본다.

### 패키지로 배포 <a id="distribute-as-a-package"></a>
`setup.py` 파일에서 `Pipenv`는 어떻게 작동할까?

위의 질문에는 많은 뉘앙스가 있다. 먼저, `Setuptools`을 빌드와 배포 시스템으로 사용하는 경우 `setup.py` 파일이 필요하다. 이것은 지금까지 사실상 표준이었지만, [최근 변경](https://www.python.org/dev/peps/pep-0518/)으로 `setuptools`의 사용이 옵션으로 되었다.

즉, [`Flit`](https://github.com/takluyver/flit)같은 프로젝트에서 새로운 `pyproject.toml`을 사용하여 `setup.py`가 아닌 다른 빌드 시스템을 지정할 수 있었다.

이러한 모든 것이, 머지 않아 `setuptools`과 함께 제공되는 `setup.py`을 많은 개발자들이 기본적으로 선택할 것이다.

패키지를 배포하는 방법으로 setup.py을 사용하는 경우 권장하는 워크플로우는 다음과 같다.

- `setup.py`
- `install_requires` 키워드는 ["정확히 실행하기 위해 필요한"](https://packaging.python.org/discussions/install-requires-vs-requirements/) 모든 패키지를 포함해야 합니다.
- `Pipfile`
- 패키지 요건의 구체적 기술
- `Pipenv` 를 사용해 패키지를 설치하여 `setup.py`의 최소 종속관계를 다음과 같이 구축한다.
    * `pipenv install =e .`를 사용
    * 그러면 `Pipfile`에 `"e1839a8" = {path = ".", editable = true}`와 같은 행을 출력한다.
    * `Pipfile.lock`
    * `pipenv lock`으로 생성된 재현 가능한 환경 세부 정보

명확하도록 `pipenv install`를 직접 실행하지 않고 `setup.py`에 최소 요건을 입력한다. 다음 `pipenv install '-e'` 명령을 사용하여 패키지를 편집 가능한 상태로 설치한다. 이를 통해 `setup.py`의 모든 요건을 배포 환경에 도입한다. 그런 다음 `pipenv lock`을 사용하여 재현 가능한 환경을 얻을 수 있다.

### 패키지 배포가 불필요한 경우 <a id="no-distribute-as-a-package"></a>
배포 또는 설치를 의도하지 않은 어플리케이션(개인 웹 사이트, 데스크톱 응용 프로그램, 게임 등)을 개발하는 경우에는 `setup.py`가 필요하지 않다.

이 경우 `Pipfile`과 `Pipfile.lock`을 사용하여 앞서 설명한 순서로 종속성을 관리하여 재현 가능한 환경을 운영 환경에 배포할 수 있다.

## requirements.txt 파일을 Pipfile 파일로 변환 <a id="convert-to-pipfile"></a>
`pipenv install`을 실행하면 자동으로 `requirements.txt`를 찾고, 다음과 같은 출력과 함께 이를 `Pipfile`로 변환한다.
```
requirements.txt found, instead of Pipfile! Converting…
Warning: Your Pipfile now contains pinned versions, if your requirements.txt did.
We recommend updating your Pipfile to specify the "*" version, instead.
```

> 위의 경고에 주의한다.

`requirements.txt` 파일에 정확한 버전이 고정되어 있는 경우, `Pipfile`을 변경하여 원하는 버전을 재지정할 수 있다. 이를 통해 전환의 진정한 이점을 얻을 수 있다. 예를 들어, 정확한 버전의 `numpy`는 필요하지 않다고 다음과 같이 가정하자.
```
[package]
numpy = "==1.14.1"
```

종속성에 대한 특정 버전 요건이 없는 경우 와일드카드 문자 `*`를 사용하여  임의의 버전을 설치할 수 있음을 Pipenv에게 알릴 수 있다.
```
[package]
numpy = "*"
```

`*`를 사용하는 것이 불안하다면, 일반적으로 현재 사용 중인 버전보다 크거나 같은 버전을 지정하는 것이 안전하다. 그러면 새 버전을 계속 활용할 수 있다.
```
[package]
numpy = ">=1.14.1"
```

물론 새로운 릴리즈로 최신 상태를 유지한다는 것은 패키지가 변경되더라도 코드가 정상적으로 작동해야 한다는 것을 의미한다. 즉, 코드에 대한 기능적 릴리스를 가능토록 하고 싶다면 이 Pipenv 절차 전체에 대한 테스트 스위트는 필수적이다.

패키지의 업데이트, 테스트 실행, 통과, 환경 잠금 등의 작업을 수행할 수 있다. 그러면 변경에 오류가 발생하지 않았음을 알 수 있기 때문에 안심할 수 있다. 종속관계로 인해 문제가 발생할 경우 작성해야 할 회귀 테스트와 종속관계로 인한 버전의 추가 제한이 있을 수 있다.

예를 들어, `pipenv install`을 실행한 후 `numpy==1.15`가 설치되고 오류가 발생함을 개발 또는 테스트 중에 알수 있다면, 다음과 같은 몇 가지 옵션이 있다.

- 종속성에 관련된 새 버전에서 작동하도록 코드를 업데이트한다.

  종속성 이전 버전과의 하위 호환성을 유지할 수 없는 경우 필요한 버전을 `Pipfile`에 포함시켜야 한다.
```
[packages]
numpy = ">=1.15"
```

- `Pipfile`내의 종속관계 패키지 버전을, `<`를 사용하여 오류가 발생된 상태의 버전으로 제한한다.
```
[package]
numpy = ">=1.14.1, <1.15"
```

첫번째 옵션을 사용하면 코드가 최신 종속성을 사용할 수 있기 때문에 이를 사용하는 것이 바람직하다. 그러나 두번째 옵션은 시간이 짧게 걸리고 코드를 변경할 필요가 없으며 종속성에 대한 제한만 있을 뿐이다.

pip 인수와 동일한 `-r` 인수를 사용하여 requirement 파일을 설치할 수도 있다.
```shell
$ pipenv install -r requirements.txt
```

`dev-requirements.txt` 또는 이와 유사한 기능이 있다면 이를 `Pipfile`에 추가할 수도 있다. `--dev` 인수를 추가하여 해당 섹션에 추가한다.
```shell
$ pipenv install -r dev-requirements.txt --dev
```

또한 다른 방법으로 `Pipfile`로부터 `requirements.txt` 파일을 생성할 수 있다.
```shell
$ pipenv lock -r > requirements.txt
$ pipenv lock -r -d > dev-requirements.txt
```

## 다음 단계 <a id="what-is-next"></a>
(PyPI 같은) 패키지 인덱스에서 패키지를 검색 및 빌드할 때 `Pipfile`을 사용하여 최소한의 종속성을 설치하고, 빌드를 지원하는 Python 에코시스템인 것 같다. [`Pipfile` 설계 사양](https://github.com/pypa/pipfile)은 아직 개발 중이고 Pipenv는 레퍼런스 구현인 점에 유념해야 한다.

향후 `setup.py`의 `install_requires` 섹션이 없고 대신 최소한의 요건을 위해 `Pipfile`을 참조하도록 할 수 있을 것이다. 또는 `setup.py`가 완전히 삭제되어 메타데이터 및 기타 정보를 다른 방법으로 얻을 수 있을 것이다. 또한 `Pipfile`을 사용하여 필요한 종속관계를 얻을 수 있다.

## 맺으며 <a id="worth-checking-out"></a>
 확실히 이미 사용하고 있는 도구(`pip`과 `virtualenv`)를 하나의 인터페이스로 통합하기 위한 방법으로서도 가치가 있다. 하지만 그 이상이다. `Pipfile`을 추가하면 실제로 필요한 종속관계만 지정할 수 있다.

개발 환경을 복제할 수 있도록 모든 버전을 직접 관리하는 번거로움이 없어졌다. `Pipfile.lock`을 사용하면 어디에서나 환경을 정확하게 재현할 수 있으므로 안심하고 개발할 수 있다.

게다가 `Pipfile` 포맷은 pip처럼 공식 Python 툴로 채택되어 지원될 가능성이 매우 높기 때문에 앞서갈 수 있다. 그리고 모든 코드를 Python 3로 업데이트하여라!

## 참고 문헌 <a id="concluding-remarks"></a>

- [Official Pipenv documentation](https://docs.pipenv.org/)
- [Official Pipfile Project](https://github.com/pypa/pipfile)
- [Issue addressing install_requires in regards to Pipfile](https://github.com/pypa/pipfile/issues/98)
- [More discussion on setup.py vs Pipfile](https://github.com/pypa/pipfile/issues/27)
- [Post talking about PEP 518](https://snarky.ca/clarifying-pep-518/)
- [Post on Python packaging](https://snarky.ca/a-tutorial-on-python-package-building/)
- [Comment suggesting Pipenv usage](https://github.com/pypa/pipenv/issues/209#issuecomment-337409290)

