# Pipenv: 새 Python 패키징 도구

[Pipenv](https://sdldocs.gitlab.io/pipenv/)가 있지만 보다 읽기 편한 문서 [Pipenv: A Guide to the New Python Packaging Tool](https://realpython.com/pipenv-guide/#whats-next)를 찾아 이를 편역하벼 보기로 하였다. 이를 먼저 읽고 난 다음, 보다 더 자세한 설명을 위하여 앞의 [문서](https://sdldocs.gitlab.io/pipenv/)를 참고하기 바란다.

이는 [Pipenv: A Guide to the New Python Packaging Tool](https://realpython.com/pipenv-guide/#whats-next)를 슈어데이터랩 환경에 적용할 수 있도록 편역한 것이다.

### 목차

- [Pipenv가 해결하려는 문제](guide/#problems)
- [Pipenv 소개](guide/#pipenv-introduction)
- [Package 배포](guide/#package-distribution)
- [requirements.txt 파일을 Pipfile 파일로 변환](guide/#convert-to-pipfile)
- [다음 단계](guide/#what-is-next)
- [사용성](guide/#worth-checking-out)
- [끝내며](guide/#concluding-remarks)

Pipenv는 [`pip`](https://realpython.com/what-is-pip/), `virtualenv` 및 `requirements.txt`을 사용하여 일반적인 워크플로우와 관련된 몇 가지 일반적인 문제를 해결하는 Python 패키징 도구이다.

몇 가지 일반적인 문제에 대처할 뿐만 아니라 개발 프로세스를 통합하고 단순화할 수 있도록 명령을 지원한다.

이 가이드에서는 `Pipenv`가 해결하는 문제와 `Pipenv`를 사용하여 [Python 의존관계를 관리](https://realpython.com/courses/managing-python-dependencies/)하는 방법에 대해 설명한다. 또한 `Pipenv`가 이전 [package](https://realpython.com/python-modules-packages/) 배포 방법과 어떻게 호환되는지 설명한다.
